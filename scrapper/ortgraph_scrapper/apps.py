from django.apps import AppConfig
from suit.apps import DjangoSuitConfig


class OrtgraphScrapperConfig(AppConfig):
    name = 'ortgraph_scrapper'


class SuitConfig(DjangoSuitConfig):
    layout = 'horizontal'
