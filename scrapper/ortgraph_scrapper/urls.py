from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from .views import MainPage, GoodPage, SectionPage

urlpatterns = \
    [
        path('', MainPage.as_view(), name='main_page'),
        path('good/<slug:good_id>/', GoodPage.as_view(), name='good_page'),
        path('section/<slug:section_name>', SectionPage.as_view(), name='section_page'),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
