from django.utils.translation import ugettext_lazy as _
from django.db.models import CharField, ForeignKey
from django.db.models import CASCADE

from ortgraph_scrapper.models import Model
from .color_group import ColorGroup


class Color(Model):
    class Meta:
        verbose_name = _('color')
        verbose_name_plural = _('colors')
        db_table = 'colors'

    color_name = CharField(_('color_name'), max_length=256, blank=True, null=True)
    color_articul = CharField(_('color_articul'), max_length=256)
    color_group = ForeignKey(ColorGroup, on_delete=CASCADE, blank=True, null=True)
    path = CharField(_('path'), max_length=1024)

    def __str__(self):
        return self.color_articul
