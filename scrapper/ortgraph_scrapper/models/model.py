from django.db import models


class Model(models.Model):
    pass

    class Meta:
        abstract = True

    created_at = models.DateTimeField(null=True, auto_now_add=True)
    updated_at = models.DateTimeField(null=True, auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
