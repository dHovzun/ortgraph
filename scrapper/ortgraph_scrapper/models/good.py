from django.utils.translation import ugettext_lazy as _
from django.db.models import CharField, TextField, ForeignKey
from django.db.models import CASCADE

from ortgraph_scrapper.models import Model
from .section import Section
from .manufacture import Manufacture


class Good(Model):
    class Meta:
        verbose_name = _('good')
        verbose_name_plural = _('goods')
        db_table = 'goods'

    good_name = CharField(_('good_name'), max_length=512)
    url = CharField(_('url'), max_length=512)
    description = TextField(_('description'), max_length=1024 * 8)
    section = ForeignKey(Section, on_delete=CASCADE, null=True, blank=True)
    manufacture = ForeignKey(Manufacture, on_delete=CASCADE, null=True, blank=True)

    def __str__(self):
        return self.good_name

    def main_photo(self):
        return self.photos.get(is_main=True)
