from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.db.models import ForeignKey
from django.db.models import CASCADE

from ortgraph_scrapper.models import Model
from ortgraph_scrapper.models import Good
from ortgraph_scrapper.models import Color


class GoodColor(Model):
    class Meta:
        verbose_name = _('good_color')
        verbose_name_plural = _('good_colors')
        db_table = 'good_colors'

    good = ForeignKey(Good, on_delete=CASCADE)
    color = ForeignKey(Color, on_delete=CASCADE)

    def __str__(self):
        return self.color.color_articul

    def color_preview(self):
        return mark_safe(u'<img src="/{0}" />'.format(self.color.path))

    color_preview.allow_tags = True
