from django.utils.translation import ugettext_lazy as _
from django.db.models import CharField, ForeignKey
from django.db.models import CASCADE

from ortgraph_scrapper.models import Model
from ortgraph_scrapper.models import Good


class GoodPdf(Model):
    class Meta:
        verbose_name = _('good_pdf')
        verbose_name_plural = _('good_pdfs')
        db_table = 'good_pdfs'

    path = CharField(_('path'), max_length=512)
    good = ForeignKey(Good, on_delete=CASCADE)
