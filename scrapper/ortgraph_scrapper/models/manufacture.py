from django.utils.translation import ugettext_lazy as _
from django.db.models import CharField

from ortgraph_scrapper.models import Model


class Manufacture(Model):
    class Meta:
        verbose_name = _('manufacture')
        verbose_name_plural = _('manufactures')
        db_table = 'manufactures'

    manufacture_name = CharField(_('manufacture_name'), max_length=256)
    logo_path = CharField(_('logo_path'), max_length=1024)

    def __str__(self):
        return self.manufacture_name
