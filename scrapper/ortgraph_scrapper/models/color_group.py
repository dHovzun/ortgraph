from django.utils.translation import ugettext_lazy as _
from django.db.models import CharField

from ortgraph_scrapper.models import Model


class ColorGroup(Model):
    class Meta:
        verbose_name = _('color_group')
        verbose_name_plural = _('color_groups')
        db_table = 'color_groups'

    group_name = CharField(_('color_name'), max_length=256)
