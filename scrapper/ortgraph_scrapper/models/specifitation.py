from django.utils.translation import ugettext_lazy as _
from django.db.models import CharField, ForeignKey
from django.db.models import CASCADE

from ortgraph_scrapper.models import Model
from ortgraph_scrapper.models import Good


class Specification(Model):
    class Meta:
        verbose_name = _('specification')
        verbose_name_plural = _('specifications')
        db_table = 'specifications'

    specification_name = CharField(_('specification_name'), max_length=512)
    specification_value = CharField(_('specification_value'), max_length=512)
    good = ForeignKey(Good, on_delete=CASCADE)

    def __str__(self):
        return self.good.good_name + ': ' + self.specification_name
