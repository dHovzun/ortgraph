from django.utils.translation import ugettext_lazy as _
from django.db.models import CharField, ForeignKey
from django.db.models import CASCADE

from ortgraph_scrapper.models import Model
from ortgraph_scrapper.models import Good


class Benefit(Model):
    class Meta:
        verbose_name = _('benefit')
        verbose_name_plural = _('benefits')
        db_table = 'benefits'

    benefit_name = CharField(_('benefit_name'), max_length=512)
    good = ForeignKey(Good, on_delete=CASCADE)

    def __str__(self):
        return self.good.good_name + ': ' + self.benefit_name
