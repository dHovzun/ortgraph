from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.db.models import CharField, ForeignKey, ImageField, BooleanField
from django.db.models import CASCADE

from ortgraph_scrapper.models import Model
from ortgraph_scrapper.models import Good


def get_upload_path(model_object, file_name):
    return '{0}/{1}/{2}'.format(
        model_object.good.manufacture.manufacture_name.replace(' ', '_').replace('/', ''),
        model_object.good.good_name.replace(' ', '_'),
        file_name
    )


fs = FileSystemStorage(location='/media/')


class GoodPhoto(Model):
    class Meta:
        verbose_name = _('good_photo')
        verbose_name_plural = _('good_photos')
        db_table = 'good_photos'

    path_old = CharField(_('path_old'), max_length=512, blank=True, null=True)
    path = ImageField(_('path'), default='', upload_to=get_upload_path)
    is_main = BooleanField(default=False)
    good = ForeignKey(Good, on_delete=CASCADE, related_name='photos')

    def photo_preview(self):
        return mark_safe(
            u'<img src="media/{0}" style="width: 120px;"/>'.format(self.path))  # todo: высчитывать размеры preview

    photo_preview.allow_tags = True
