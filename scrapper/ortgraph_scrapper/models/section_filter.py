from django.utils.translation import ugettext_lazy as _
from django.db.models import CharField, ForeignKey
from django.db.models import CASCADE

from ortgraph_scrapper.models import Model
from ortgraph_scrapper.models import Section


class SectionFilter(Model):
    class Meta:
        verbose_name = _('section_filter')
        verbose_name_plural = _('section_filters')
        db_table = 'section_filters'

    filter_name = CharField(_('filter_name'), max_length=256)
    filter_value = CharField(_('filter_value'), max_length=512)
    section = ForeignKey(Section, on_delete=CASCADE)

