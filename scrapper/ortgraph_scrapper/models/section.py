from django.utils.translation import ugettext_lazy as _
from django.db.models import CharField

from ortgraph_scrapper.models import Model


class Section(Model):
    class Meta:
        verbose_name = _('section')
        verbose_name_plural = _('sections')
        db_table = 'sections'

    section_name = CharField(_('section_name'), max_length=256)
    russian_name = CharField(_('russian_name'), max_length=256, default='')
    section_url = CharField(_('section_url'), max_length=512)

    def __str__(self):
        return self.section_name
