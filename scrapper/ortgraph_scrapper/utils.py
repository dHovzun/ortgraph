def dict_exclude(data, keys):
    result = {}

    for key, value in data.items():
        if key not in keys:
            result[key] = data[key]

    return result


def dict_only(data, keys):
    result = {}

    for key, value in data.items():
        if key in keys:
            result[key] = data[key]

    return result
