from django.views import View
from django.shortcuts import render
from ortgraph_scrapper.repositories import *


class GoodPage(View):
    template_name = 'good_page.html'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.good_repository = GoodsRepository()
        self.section_repository = SectionsRepository()
        self.photos_repository = GoodPhotosRepository()
        self.benefits_repository = BenefitsRepository()
        self.colors_repository = GoodColorsRepository()
        self.specifications_repository = SpecificationsRepository()
        self.sections = self.section_repository.all()

    def get(self, request, good_id):
        good = self.good_repository.get_or_not_found(**{'id': good_id})
        photos = self.photos_repository.get_by_fields(**{'good': good})
        benefits = self.benefits_repository.get_by_fields(**{'good': good})
        colors = self.colors_repository.get_by_fields(**{'good': good})
        specifications = self.specifications_repository.get_by_fields(**{'good': good})

        return render(request, self.template_name, {
            'good': good,
            'photos': photos,
            'benefits': benefits,
            'colors': colors,
            'specifications': specifications,
            'sections': self.sections
        })
