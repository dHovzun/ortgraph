from django.views import View
from django.shortcuts import render
from ortgraph_scrapper.repositories import *


class MainPage(View):
    template_name = 'main_page.html'
    per_page = 6

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.good_repository = GoodsRepository()
        self.section_repository = SectionsRepository()
        self.sections = self.section_repository.all()

    def get(self, request):
        kovrolin = self.good_repository.get_newest('kovrolin', self.per_page)

        return render(request, self.template_name, {
            'kovrolin': kovrolin,
            'sections': self.sections
        })
