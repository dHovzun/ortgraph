import datetime
from time import sleep

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException


class Scrapper:
    base_url = 'https://ortgraph.ru'
    query = '?PAGEN_1={0}'

    options = None
    browser = None
    request_url = None
    url = None
    selectors = {}

    def __init__(self, url, selectors):
        self.options = Options()
        self.options.headless = True
        self.browser = Chrome(options=self.options)
        self.request_url = self.base_url + url
        self.url = url
        self.selectors = selectors
        self.browser.get(self.request_url)
        sleep(2)

    def close_scrapper(self):
        self.browser.close()

    def parse_items(self, pages_count):
        print('Parsing items...')
        data = []

        if pages_count:
            for index in range(1, int(pages_count) + 1):
                print(datetime.datetime.now().strftime("%H:%M:%S") + ':  parsing {0} page...'.format(index))
                query = self.query.format(index)
                self.browser.get(self.request_url + query)
                print('start parsing links...')
                sleep(2)
                data = self.__parse_items_from_page(self.__get_links_from_page())

        else:
            data = self.__parse_items_from_page(self.__get_links_from_page())

        return data

    def __parse_items_from_page(self, item_links):
        items = []

        for item_link in item_links:
            self.browser.get(item_link)
            print('     parsing page with url = {0}'.format(item_link))
            sleep(3)
            items.append(self.__parse_item())

        return items

    def parse_pages_count(self):
        return self.__process_element(self.browser, self.selectors.get('pages_count'))

    def __parse_item(self):
        if self.__page_was_loaded():
            good_name = self.__process_element(self.browser, self.selectors.get('item_name'))

            return {
                'url': self.browser.current_url,
                'description': self.__process_element(self.browser, self.selectors.get('item_description')).strip(),
                'manufactures': self.__parse_manufacture(),
                'benefits': self.__parse_benefits(),
                'photos': self.__parse_photos(),
                'specifications': self.__parse_specifications(),
                'colors': self.__parse_colors(),
                'section': self.url.replace('/', ''),
                'good_name': good_name.strip(),
                'pdfs': self.__parse_pdfs()
            }
        else:
            sleep(3)
            self.__parse_item()

    def __parse_photos(self):
        photos = self.__process_element(self.browser, self.selectors.get('item_photos'), True)

        return [{'path': self.base_url + photo.get_attribute('data-origin')} for photo in photos]

    def __parse_benefits(self):
        benefits = self.__process_element(self.browser, self.selectors.get('item_benefits'), True)

        return [{'benefit_name': benefit.text} for benefit in benefits]

    def __parse_specifications(self):
        self.browser.find_element_by_css_selector(self.selectors.get('item_specifications_button')).click()
        sleep(1)
        specifications = self.__process_element(self.browser, self.selectors.get('item_specifications'), True)
        result = []

        for specification in specifications:
            result.append({
                'specification_name': self.__process_element(specification, 'td:first-child').replace(':', ''),
                'specification_value': self.__process_element(specification, 'td:last-child')
            })

        return result

    def __parse_pdfs(self):
        result = {}
        button = self.browser.find_element_by_css_selector(self.selectors.get('item_pdfs_button'))

        if button.text == 'Скачать':
            button.click()
            sleep(1)
            result['path'] = self.__process_element(self.browser, self.selectors.get('item_pdfs'), False, 'href')

        return result

    def __parse_manufacture(self):
        return {
            'logo_path': self.__process_element(self.browser, self.selectors.get('item_manufacture_logo'), False,
                                                'src'),
            'manufacture_name': self.__process_element(self.browser, self.selectors.get('item_manufacture_name'))
        }

    def __parse_colors(self):
        colors = self.__process_element(self.browser, self.selectors.get('item_colors'), True)
        result = []

        for color in colors:
            result.append({
                'color_articul': self.__process_element(color, self.selectors.get('item_color'), False, 'data-articul'),
                'path': self.__process_element(color, self.selectors.get('item_color_path'), False, 'src')
            })

        return result

    @staticmethod
    def __process_element(context, selector, bulk=False, attribute=''):
        method_name = 'find_elements_by_css_selector' if bulk else 'find_element_by_css_selector'
        method_to_call = getattr(context, method_name)

        try:
            return method_to_call(selector).get_attribute(attribute) \
                if attribute \
                else method_to_call(selector) if bulk else method_to_call(selector).text
        except NoSuchElementException:
            return ''

    def __get_links_from_page(self):
        links = self.__process_element(self.browser, self.selectors.get('item_link'), True)

        return [link.get_attribute('href') for link in links]

    def __page_was_loaded(self):
        page_state = self.browser.execute_script('return document.readyState;')

        return page_state == 'complete'
