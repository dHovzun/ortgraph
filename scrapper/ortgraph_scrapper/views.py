from django.shortcuts import render
from django.core.paginator import Paginator
from ortgraph_scrapper.repositories import *


def main_page(request):
    goods_repository = GoodsRepository()
    section_repository = SectionsRepository()
    paginator = Paginator(goods_repository.all(), 10)

    page = request.GET.get('page')
    goods = paginator.get_page(page)

    return render(request, 'main_page.html', {
        'goods': goods,
        'sections': section_repository.all()
    })


def section_page(request, section_name):
    goods_repository = GoodsRepository()
    section_repository = SectionsRepository()
    section = section_repository.get_by_field_value(section_name, 'section_name')
    paginator = Paginator(goods_repository.get_by_fields(**{'section_id': section.id}), 10)

    page = request.GET.get('page')
    goods = paginator.get_page(page)

    return render(request, 'main_page.html', {
        'goods': goods,
        'sections': section_repository.all()
    })


def good_page(request, section_name, manufacture_id, good_id):
    goods_repository = GoodsRepository()
    section_repository = SectionsRepository()
    manufacture_repository = ManufacturesRepository()
    specification_repository = SpecificationsRepository()
    benefit_repository = BenefitsRepository()
    pdf_repository = GoodPdfsRepository()
    photos_repository = GoodPhotosRepository()

    good = goods_repository.get_or_not_found(**{
        'id': good_id,
        'section': section_repository.get_or_not_found(**{'section_name': section_name}),
        'manufacture': manufacture_repository.get_or_not_found(**{'id': manufacture_id})
    })
    specifications = specification_repository.get_by_fields(**{'good': good})
    benefits = benefit_repository.get_by_fields(**{'good': good})
    pdf = pdf_repository.get_by_field_value(good, 'good')

    return render(request, 'good_page.html', {
        'good': good,
        'specifications': specifications,
        'benefits': benefits,
        'pdf': pdf,
        'photos': photos_repository.get_by_fields(**{'good': good}),
        'sections': section_repository.all()
    })
