from ortgraph_scrapper.repositories import Repository
from ortgraph_scrapper.models import GoodPhoto


class GoodPhotosRepository(Repository):
    def __init__(self):
        self._set_model(GoodPhoto)
