from ortgraph_scrapper.repositories import Repository
from ortgraph_scrapper.models import ColorGroup


class ColorGroupsRepository(Repository):
    def __init__(self):
        self._set_model(ColorGroup)
