from ortgraph_scrapper.repositories import Repository
from ortgraph_scrapper.models import SectionFilter


class SectionFiltersRepository(Repository):
    def __init__(self):
        self._set_model(SectionFilter)
