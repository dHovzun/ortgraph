from ortgraph_scrapper.repositories import Repository
from ortgraph_scrapper.models import Specification


class SpecificationsRepository(Repository):
    def __init__(self):
        self._set_model(Specification)
