from ortgraph_scrapper.repositories import Repository
from ortgraph_scrapper.models import Manufacture


class ManufacturesRepository(Repository):
    def __init__(self):
        self._set_model(Manufacture)
