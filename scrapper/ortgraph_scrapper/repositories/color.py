from ortgraph_scrapper.repositories import Repository
from ortgraph_scrapper.models import Color


class ColorsRepository(Repository):
    def __init__(self):
        self._set_model(Color)
