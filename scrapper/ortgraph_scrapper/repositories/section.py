from ortgraph_scrapper.repositories import Repository
from ortgraph_scrapper.models import Section


class SectionsRepository(Repository):
    def __init__(self):
        self._set_model(Section)
