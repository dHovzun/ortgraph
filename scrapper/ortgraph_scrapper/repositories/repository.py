from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404


class Repository:
    model = None

    search_fields = []

    def _set_model(self, model):
        self.model = model

    def create(self, data, *args, **kwargs):
        model_object = self.model(**data)
        model_object.save()

        return self.get_by_field_value(model_object.pk)

    def bulk_create(self, data, *args, **kwargs):
        for item in data:
            self.create(item)

    def update(self, where, data):
        model_object = self.model.objects.get(**where)

        for attr, value in data.items():
            setattr(model_object, attr, value)

        model_object.save()

        return model_object

    def get_by_field_value(self, value, field='pk'):
        try:
            return self.model.objects.get(**{field: value})
        except ObjectDoesNotExist:
            return None

    def get_by_fields(self, **fields):
        return self.model.objects.filter(**fields)

    def exists(self, value, field='pk', with_deleted=False):
        objects = self.model.all_objects if with_deleted else self.model.objects

        return objects.filter(**{field: value}).exists()

    def latest(self, field='id'):
        return self.model.objects.latest(field)

    def all(self):
        return self.model.objects.all()

    def get_or_not_found(self, **fields):
        return get_object_or_404(self.model, **fields)
