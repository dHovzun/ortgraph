from ortgraph_scrapper.repositories import Repository
from ortgraph_scrapper.models import Benefit


class BenefitsRepository(Repository):
    def __init__(self):
        self._set_model(Benefit)
