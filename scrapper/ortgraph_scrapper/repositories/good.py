from ortgraph_scrapper.repositories import Repository
from ortgraph_scrapper.models import Good


class GoodsRepository(Repository):
    def __init__(self):
        self._set_model(Good)

    def get_newest(self, section_name, objects_count):
        return self.get_by_fields(**{'section__section_name': section_name}).order_by('-created_at')[:objects_count]
