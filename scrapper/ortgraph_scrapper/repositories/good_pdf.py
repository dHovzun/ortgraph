from ortgraph_scrapper.repositories import Repository
from ortgraph_scrapper.models import GoodPdf


class GoodPdfsRepository(Repository):
    def __init__(self):
        self._set_model(GoodPdf)
