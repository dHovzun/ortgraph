from ortgraph_scrapper.repositories import Repository
from ortgraph_scrapper.models import GoodColor


class GoodColorsRepository(Repository):
    def __init__(self):
        self._set_model(GoodColor)
