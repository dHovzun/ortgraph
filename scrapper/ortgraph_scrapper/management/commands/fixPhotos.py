from os import listdir
from os.path import isfile, join, isdir
from PIL import Image

from django.core.files import File
from django.core.management import BaseCommand
from ortgraph_scrapper.repositories import *


class Command(BaseCommand):
    help = 'Parse goods'

    def __init__(self):
        super().__init__()
        self.good_photos_repository = GoodPhotosRepository()
        self.manufactures_repository = ManufacturesRepository()

    def handle(self, *args, **options):
        dirs = [directory for directory in listdir('media/') if isdir('media/' + directory) and directory != 'colors']

        for directory in dirs:
            manufacture_name = directory.replace('__', ' / ').replace('_', ' ')

            if not self.manufactures_repository.exists(manufacture_name, 'manufacture_name'):
                logo_path = 'media/' + directory + '/' + [logo for logo in listdir('media/' + directory) if
                                                          isfile('media/' + directory + '/' + logo)][0]
                self.manufactures_repository.create({
                    'manufacture_name': manufacture_name,
                    'logo_path': logo_path
                })

            print('media/' + directory)
            good_dir_names = [print(good_name) for good_name in listdir('media/' + directory) if isdir(good_name)]

            for good_dir_name in good_dir_names:
                good_name = good_dir_name.replace('_', ' ')
                good_photos_path = 'media/' + directory + '/' + good_name
                photos = [photo for photo in listdir(good_photos_path) if isfile(good_photos_path + '/' + photo)]

                for photo_number in range(0, len(photos)):
                    data_to_save = {'path': good_photos_path + '/' + photos[photo_number]}

                    if photo_number == 0:
                        data_to_save['main'] = True

                    self.good_photos_repository.update(
                        {'good__good_name': good_name},
                        data_to_save
                    )

# photos = self.good_photos_repository.all()
#
# for photo in photos:
#     print('Имя и id объекта: имя --{0}; id -- {1}'.format(photo.good.good_name, photo.good.id))
#     path = 'media/' + photo.good.manufacture.manufacture_name.replace(' ', '_').replace('/', '') + '/' \
#            + photo.good.good_name.replace(' ', '_')
#     files = [f for f in listdir(path) if isfile(join(path, f))]
#
#     for file in files:
#         file_to_save = open(path + '/' + file, 'rb')
#         # print(' Trying to update file={0}'.format(file))
#         # new_field = File(file_to_save)
#         photo.path = path + '/' + file
#         photo.save()
#         file_to_save.close()
