from ortgraph_scrapper.scrappers import Scrapper
from ortgraph_scrapper.services import *
from ortgraph_scrapper.repositories import *
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Parse goods'
    selectors = {
        'item_name': 'h1',
        'item_description': '.card-preview-text',
        'item_photos': '.card-carousel .item a',
        'item_benefits': '.features ul span',
        'item_specifications': '.table-simple table tbody tr',
        'item_specifications_button': '.product-tabs ul li:nth-child(2)',
        'item_manufacture_logo': '.brand-img img',
        'item_manufacture_name': '.card-logo a',
        'item_colors': '.offer-list ul',
        'item_color': '.offer-list ul .offer_index',
        'item_color_path': '.offer_index img',
        'item_link': '.product-item h3 a',
        'pages_count': '.modern-page-navigation a:nth-last-child(2)',
        'item_pdfs_button': '.product-tabs ul li:last-child',
        'item_pdfs': '#tab-files a'
    }

    def __init__(self):
        super().__init__()
        self.section_repository = SectionsRepository()
        self.goods_service = GoodsService()

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_id', nargs='+', type=int)

    def handle(self, *args, **options):
        self.goods_service.import_goods()
