from ortgraph_scrapper.services import Service
from ortgraph_scrapper.repositories import ManufacturesRepository


class ManufacturesService(Service):
    def __init__(self):
        self._set_repository(ManufacturesRepository)

    def create(self, data, manufacture_name=''):
        if not self.repository.exists(data.get('manufacture_name'), 'manufacture_name') \
                and data.get('logo_path') != '':
            file_path = self.save_file(data.get('logo_path'), data.get('manufacture_name').replace('/', ''))
            data.update({'logo_path': file_path})
            self.repository.create(data)
