from ortgraph_scrapper.services import Service
from ortgraph_scrapper.repositories import SpecificationsRepository


class SpecificationsService(Service):
    def __init__(self):
        self._set_repository(SpecificationsRepository)

    def create(self, specifications, manufacture_name=''):
        for specification in specifications:
            self.repository.create(specification)
