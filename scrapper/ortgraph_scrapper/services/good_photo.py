from ortgraph_scrapper.services import Service
from ortgraph_scrapper.repositories import GoodPhotosRepository


class GoodPhotosService(Service):
    def __init__(self):
        self._set_repository(GoodPhotosRepository)

    def create(self, photos, manufacture_name):
        for photo in photos:
            path = self.save_file(photo.get('path'),
                                  manufacture_name.replace('/', '') + '/' + photo.get('good').good_name + '/')
            photo['path'] = path
            self.repository.create(photo)
