from ortgraph_scrapper.services import Service
from ortgraph_scrapper.repositories import SectionFiltersRepository


class SectionFiltersService(Service):
    def __init__(self):
        self._set_repository(SectionFiltersRepository)
