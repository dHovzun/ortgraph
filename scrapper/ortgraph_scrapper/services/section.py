from ortgraph_scrapper.services import Service
from ortgraph_scrapper.repositories import SectionsRepository


class SectionsService(Service):
    def __init__(self):
        self._set_repository(SectionsRepository)
