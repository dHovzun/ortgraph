import os
import requests

from scrapper.settings import BASE_DIR


class Service:
    repository = None

    def _set_repository(self, repository):
        self.repository = repository()

    def __getattr__(self, name):
        def check_method(*args, **kwargs):
            method = getattr(self.repository, name, None)

            if callable(method):
                return method(*args, *kwargs)

        return check_method

    @staticmethod
    def save_file(file_url, location, need_slash_replace=True):
        # location = location.replace('/', '') if need_slash_replace else location
        location = location.replace(' ', '_')

        if not os.path.exists(os.path.join(BASE_DIR, 'media/' + location)):
            os.mkdir(os.path.join(BASE_DIR, 'media/' + location))

        file = requests.get(file_url)
        file_name = file_url.rsplit('/', 1)[-1]
        open(os.path.join(BASE_DIR, 'media/' + location + '/' + file_name), 'wb').write(file.content)

        return 'media/' + location + '/' + file_name
