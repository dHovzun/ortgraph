from ortgraph_scrapper.services import Service
from ortgraph_scrapper.repositories import GoodsRepository


class ColorsService(Service):
    def __init__(self):
        self._set_repository(GoodsRepository)
