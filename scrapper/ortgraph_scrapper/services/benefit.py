from ortgraph_scrapper.services import Service
from ortgraph_scrapper.repositories import BenefitsRepository


class BenefitsService(Service):
    def __init__(self):
        self._set_repository(BenefitsRepository)

    def create(self, benefits, manufacture_name=''):
        for benefit in benefits:
            self.repository.create(benefit)
