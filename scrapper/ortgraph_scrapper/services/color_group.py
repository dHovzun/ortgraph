from ortgraph_scrapper.services import Service
from ortgraph_scrapper.repositories import ColorGroupsRepository


class ColorGroupsService(Service):
    def __init__(self):
        self._set_repository(ColorGroupsRepository)
