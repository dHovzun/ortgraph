from ortgraph_scrapper.services import Service
from ortgraph_scrapper.repositories import GoodPdfsRepository


class GoodPdfsService(Service):
    def __init__(self):
        self._set_repository(GoodPdfsRepository)

    def create(self, pdf, manufacture_name):
        if pdf.get('path') is not None:
            path = self.save_file(
                pdf.get('path'),
                manufacture_name.replace('/', '') + '/' + pdf.get('good').good_name + '/pdfs/'
            )

            pdf['path'] = path
            self.repository.create(pdf)
