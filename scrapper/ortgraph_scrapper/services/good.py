from ortgraph_scrapper.repositories import GoodsRepository
from .section import SectionsService
from .service import Service
from .manufacture import ManufacturesService
from .good_photo import GoodPhotosService
from .good_color import GoodColorsService
from .benefit import BenefitsService
from .specification import SpecificationsService
from .good_pdf import GoodPdfsService
from ortgraph_scrapper.utils import *
from ortgraph_scrapper.scrappers import Scrapper


class GoodsService(Service):
    selectors = {
        'item_name': 'h1',
        'item_description': '.card-preview-text',
        'item_photos': '.card-carousel .item a',
        'item_benefits': '.features ul span',
        'item_specifications': '.table-simple table tbody tr',
        'item_specifications_button': '.product-tabs ul li:nth-child(2)',
        'item_manufacture_logo': '.brand-img img',
        'item_manufacture_name': '.card-logo a',
        'item_colors': '.offer-list ul',
        'item_color': '.offer-list ul .offer_index',
        'item_color_path': '.offer_index img',
        'item_link': '.product-item h3 a',
        'pages_count': '.modern-page-navigation a:nth-last-child(2)',
        'item_pdfs_button': '.product-tabs ul li:last-child',
        'item_pdfs': '#tab-files a'
    }

    def __init__(self):
        self._set_repository(GoodsRepository)

        self.photos_service = GoodPhotosService()
        self.colors_service = GoodColorsService()
        self.benefits_service = BenefitsService()
        self.specifications_service = SpecificationsService()
        self.sections_service = SectionsService()
        self.manufactures_service = ManufacturesService()
        self.pdfs_service = GoodPdfsService()

    def create(self, data):
        additional_data_keys = ('manufactures', 'benefits', 'specifications', 'photos', 'colors', 'pdfs')
        good_data = dict_exclude(data, additional_data_keys)
        good_data = self.__add_section(good_data, data.get('section'))
        self.repository.create(good_data)
        latest_good = self.repository.latest()
        manufacture_name = data.get('manufactures').get('manufacture_name')

        for key in additional_data_keys:
            if data.get(key) is not None:
                data_to_save = self.__add_good(data.get(key), latest_good) if key != 'manufactures' else data.get(key)
                getattr(self, key + '_service').create(data_to_save, manufacture_name)

        manufacture = self.manufactures_service.get_by_field_value(data.get('manufactures').get('manufacture_name'),
                                                                   'manufacture_name')
        self.repository.update(latest_good.id, {'manufacture': manufacture})

    def bulk_create(self, goods):
        for good in goods:
            if not self.repository.exists(good.get('good_name'), 'good_name'):
                self.create(good)

    def import_goods(self):
        sections = self.sections_service.all()

        for section in sections:
            scrapper = Scrapper('/' + section.section_name + '/', self.selectors)
            pages_count = scrapper.parse_pages_count()
            print(pages_count)
            data = scrapper.parse_items(pages_count)

            for item in data:
                manufacture_check = {
                    'manufacture__manufacture_name': item.get('manufactures').get('manufacture_name')
                }
                good_check = {'good_name': item.get('good_name'), 'manufacture': None}

                if not self.repository.exists(manufacture_check) or self.repository.exists(good_check):
                    print('     saving item with name={0} and manufacture={1} to db'.format(
                        item.get('good_name'),
                        item.get('manufacture_name')
                    ))
                    self.create(item)
            # scrapper.close_scrapper()

    @staticmethod
    def __add_good(data, good):
        if isinstance(data, list):
            for item in data:
                item['good'] = good
        else:
            data.update({'good': good})

        return data

    def __add_section(self, good, section_name):
        section = self.sections_service.get_by_field_value(section_name, 'section_name')
        good.update({'section': section})

        return good
