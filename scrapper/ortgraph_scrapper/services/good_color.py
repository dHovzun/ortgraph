from ortgraph_scrapper.services import Service
from ortgraph_scrapper.repositories import GoodColorsRepository, ColorsRepository

from ortgraph_scrapper.utils import *


class GoodColorsService(Service):
    def __init__(self):
        self._set_repository(GoodColorsRepository)
        self.color_repository = ColorsRepository()

    def create(self, colors, manufacture_name):
        for color in colors:
            if not self.color_repository.exists(color.get('color_articul'), 'color_articul') \
                    and color.get('path') != '':
                path = self.save_file(color.get('path'), 'colors')
                color.update({'path': path})
                created_color = self.color_repository.create(dict_exclude(color, ['good']))
                self.repository.create({
                    'color': created_color,
                    'good': color.get('good')
                })
