from django.contrib import admin

from .models import *

admin.site.register(Color)
admin.site.register(Manufacture)
admin.site.register(Section)
admin.site.register(ColorGroup)


class GoodPdfsInline(admin.TabularInline):
    model = GoodPdf
    extra = 1
    exclude = ['deleted_at']


class GoodPhotosInline(admin.TabularInline):
    model = GoodPhoto
    extra = 1
    exclude = ['deleted_at']

    fields = ['path']
    readonly_fields = ['photo_preview', ]


class GoodColorsInline(admin.TabularInline):
    model = GoodColor
    extra = 1
    exclude = ['deleted_at']
    fields = ['color_preview', ]
    readonly_fields = ['color_preview', ]


class SpecificationsInline(admin.TabularInline):
    model = Specification
    extra = 1
    exclude = ['deleted_at']


class BenefitsInline(admin.TabularInline):
    model = Benefit
    extra = 1
    exclude = ['deleted_at']


class GoodAdmin(admin.ModelAdmin):
    inlines = [
        SpecificationsInline,
        BenefitsInline,
        GoodPhotosInline,
        GoodColorsInline,
        GoodPdfsInline,
    ]
    exclude = ['deleted_at']

    search_fields = [
        'good_name'
    ]

    list_filter = [
        'manufacture__manufacture_name',
        'section__section_name'
    ]


admin.site.register(Good, GoodAdmin)
